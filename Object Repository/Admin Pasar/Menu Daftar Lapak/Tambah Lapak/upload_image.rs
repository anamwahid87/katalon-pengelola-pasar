<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>upload_image</name>
   <tag></tag>
   <elementGuidId>c16a8904-52e7-48a1-9e84-34acd5d9bc5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;attach_img_lapak&quot;][count(. | //*[@id = 'attach_img_lapak']) = count(//*[@id = 'attach_img_lapak'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;attach_img_lapak&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>attach_img_lapak</value>
   </webElementProperties>
</WebElementEntity>
