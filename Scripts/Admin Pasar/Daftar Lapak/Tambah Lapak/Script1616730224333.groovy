import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandStr

WebUI.callTestCase(findTestCase('Admin Pasar/Daftar Lapak/List Lapak'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Admin Pasar/Menu Daftar Lapak/btn_tambah_lapak'))

WebUI.waitForPageLoad(5)

rand_number = RandStr.randomNumeric(5)

'INPUT NAMA LAPAK'
WebUI.setText(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/input_nama'), 'Lapak' + rand_number + 'testing')

'INPUT KODE LAPAK'
WebUI.setText(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/input_kodelapak'), 'test' + rand_number)

'INPUT DESKRIPSI'
WebUI.setText(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/input_deskripsi'), 'menjual segala kebutuhan anda')

'PILIH PEDAGANG'
WebUI.click(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/dropdown_pedagang'))

WebUI.click(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/input_pedagang_ashri'))

'PILIH KATEGORI'
WebUI.click(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/dropdown_kategori'))

WebUI.click(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/pilih_kategori_buah'))

WebUI.uploadFile(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/upload_image'), 'D:\\bg.png')

WebUI.scrollToElement(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/btn_submit'), 3)

WebUI.click(findTestObject('Admin Pasar/Menu Daftar Lapak/Tambah Lapak/btn_submit'))

WebUI.waitForPageLoad(9)

