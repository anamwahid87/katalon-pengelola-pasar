import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandStr

WebUI.callTestCase(findTestCase('Admin Pasar/Daftar Kategori/List Kategori'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Admin Pasar/Menu Daftar Kategori/btn_tambah_kategori'))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Object Repository/Admin Pasar/Menu Daftar Kategori/Tambah Kategori/dropdown_kategori'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Admin Pasar/Menu Daftar Kategori/Tambah Kategori/dropdown_kategori'), 
    '15', false)

WebUI.click(findTestObject('Object Repository/Admin Pasar/Menu Daftar Kategori/Tambah Kategori/dropdown_urutan'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Admin Pasar/Menu Daftar Kategori/Tambah Kategori/dropdown_urutan'), 
    '15', false)

WebUI.click(findTestObject('Object Repository/Admin Pasar/Menu Daftar Kategori/Tambah Kategori/btn_submit'))

WebUI.verifyTextNotPresent('Gagal menambahkan kategori baru. Pastikan kategori belum terdaftar', false, FailureHandling.OPTIONAL)

WebUI.verifyElementPresent(findTestObject('Admin Pasar/Menu Daftar Kategori/btn_tambah_kategori'), 0, FailureHandling.OPTIONAL)

