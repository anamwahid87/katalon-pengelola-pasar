package random.date

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import com.kms.katalon.core.annotation.Keyword
import java.util.Random

class Functions{
	@Keyword
	public static LocalDate getDateBefore(LocalDate date=LocalDate.now(), int maxDayDifference=1000){
		Random rand = new Random()
		int days = rand.nextInt(maxDayDifference)
		return date.minusDays(days)
	}

	@Keyword
	public static String getDateString(LocalDate date, String format="YYYY/MM/dd"){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		String formattedDateTime = date.format(formatter);
		return formattedDateTime
	}
}